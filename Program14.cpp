#include <iostream>
 
int main() {   
    int a,b,c;
    bool done = false;
    do {   
        std::cout << "Enter first number" << std::endl;
        std::cin >> a;
        std::cout << "Enter second number" << std::endl;
        std::cin >> b;    
        try {
            if (b == 0)
            throw "error";
            c = a/b;
            std::cout <<"Answer is : "<< c << std::endl;
            done = true;
        }
        catch(...) {
            std::cout << "Error: Division by zero not allowed."<<std::endl;
        }
    }
    while (! done);
    return 0;
}


