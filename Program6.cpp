#include <iostream>

// Forward declaration
class A;

class B {
    private:
        double data;
    public:
        B(double x) {
            data = x;
        }
        friend double mean(A &a, B &b);
};

class A {
    private:
        double data;
    public:
        A(double x) {
            data = x;
        }
        friend double mean(A &a, B &b);
};

double mean(A &a, B &b) {
    double m = (a.data + b.data)/2.0;
    return m;
}

int main() {
    A a = A(10.0);
    B b = B(20.0);
    std::cout << "Mean = " << mean(a, b) << std::endl;
    return 0;
}