#include <iostream>

class Sample {
    private:
        int a;
        int b;
    public:
        Sample(int x, int y) {
            this -> a = x;
            this -> b = y;
        }
        void showValues() {
            std::cout << "a = " << a << ", b = " << b << std::endl;
        }
        friend void swap(Sample&);
};

void swap(Sample &s) {
    int Sample::*ptrA = &Sample::a;
    int Sample::*ptrB = &Sample::b;
    void (Sample::*ptrShowValues) () = &Sample::showValues;
    std::cout << "Before swapping: ";
    (s.*ptrShowValues)();
    int temp = s.*ptrA;
    s.*ptrA = s.*ptrB;
    s.*ptrB = temp;
    std::cout << "After swapping: ";
    (s.*ptrShowValues)();
}

int main() {
    Sample s = Sample(1, 2);
    swap(s);
    return 0;
}