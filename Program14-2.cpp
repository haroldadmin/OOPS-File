# include <iostream>
int main () {
    try {
        char * mystring;
        mystring = new char [10];
        if (mystring == NULL) throw "Allocation failure";
        for (int n=0; n<=100; n++) {
            if (n>9) throw n;
            mystring[n]='z';
        }
    }
    catch (int i) {
        std::cout << "Exception: ";
        std::cout << "index " << i << " is out of range" << std::endl;  
    }
    catch (char * str) { 
        std::cout << "Exception: " << str << std::endl;   
    }
    return 0;
}