#include <iostream>

class Circle {
    private:
        double radius;
    public:
        Circle() {
            radius = 0.0;
        }
        double get_radius();
        void set_radius(double);
        double get_area();
};

double Circle::get_radius() {
    return this->radius;
}

void Circle::set_radius(double r) {
    this->radius = r;
}

double Circle::get_area() {
    double area = 3.14 * radius * radius;
    return area;
}

int main() {
    Circle c;
    std::cout << "Enter radius for the circle: " << std::endl;
    double r;
    std::cin >> r;
    c.set_radius(r);
    std::cout << "Radius of circle: " << c.get_radius() << std::endl;
    std::cout << "Area of the circle: " << c.get_area() << std::endl;
    return 0;
}
