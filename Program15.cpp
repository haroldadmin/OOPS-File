#include<iostream>
#include<fstream>
int main() {
    std::ifstream fin;
    char filename[20];
    std::cout<< "Enter filename: ";
    gets(filename);
    fin.open(filename);
    int line=0,word=0,chars=0;
    char ch;
    fin.seekg(0,std::ios::end);
    fin.seekg(0,std::ios::beg);
    while(fin){
        fin.get(ch);
        if(ch!=' ' && ch!='\n') ++chars;
        if(ch==' '|| ch=='\n') ++word;
        if(ch=='\n')  ++line;
    }
    std::cout << "Sentences= " << line << "\nWords= "<< word << "\nCharacters= " << chars << std::endl;
    fin.close(); // closing file
    return 0;
}
