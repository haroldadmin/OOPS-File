#include <iostream>

double volume(double);
double volume(double, double, double);
double volume(double, double);

int main() {
    std::cout << "Enter choice for volume calculation: " << std::endl;
    std::cout << " 1.Cube\t 2.Cuboid\t 3.Cone" << std::endl;
    int choice;
    std::cin >> choice;
    double vol;
    switch(choice) {
        case 1:
            double side;
            std::cout << "Enter side length of cube: " << std::endl;
            std::cin >> side;
            vol = volume(side);
            break;
        case 2:
            double l, b, h;
            std::cout << "Enter length, breadth, and height of cuboid: " << std::endl;
            std::cin >> l >> b >> h;
            vol = volume(l, b, h);
            break;
        case 3:
            double radius, height;
            std::cout << "Enter radius and height of cone: " << std::endl;
            std::cin >> radius >> height;
            vol = volume(radius, height);
            break;
    }
    std::cout << "Calculated volume: " << vol << std::endl;
    return 0;
}

double volume(double side) {
    return side*side*side;
}

double volume(double l, double b, double h) {
    return l*b*h;
}

double volume(double r, double h) {
    return (3.14 * r * r * h);
}