#include<iostream>

class A {
    protected:
        int a, b ;
    public:
        void getab() {
            std::cout << "Enter a and b value : ";
            std::cin >> a >> b; 
        }
};

class B: public A {
    protected:
        int c ;
    public:
        void getc() {
            std::cout << "Enter c value : ";
            std::cin >> c; 
        }
};
class C {
    protected:
        int d ;
    public:
        void getd() {
        std::cout << "Enter d value : ";
        std::cin >> d; 
        }
};
class D: public B, public C {
    protected:
        int e ;
    public:
        void result() {
            getab(); 
            getc();
            getd(); 
            e=a+b+c+d;
            std::cout << "Addition is : " << e << std::endl; 
        }
};

int main() {
    D d1;
    d1.result();
    return 0;
}