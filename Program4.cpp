#include <iostream>

class Employee {
    private:
        char name[50];
        int age;
        int salary;
        int id;
    public:
        void set_details(int);
        void get_details(int);
};

void Employee::set_details(int number) {
    std::cout << "~~~~~Accepting details for employee " << number << "~~~~~" <<std::endl;
    std::cout << "Enter name " << std::endl;
    std::cin >> this->name;
    std::cout << "Enter age " << std::endl;
    std::cin >> this->age;
    std::cout << "Enter salary " << std::endl;
    std::cin >> this->salary;
    std::cout << "Enter employee id " << std::endl;
    std::cin >> this->id;
}

void Employee::get_details(int number) {
    std::cout << "*****Displaying details for employee " << number << "*****" <<std::endl;
    std::cout << "Name:\t" << this->name << std::endl;
    std::cout << "Age:\t" << this->age << std::endl;
    std::cout << "Salary:\t" << this->salary << std::endl;
    std::cout << "ID:\t" << this->id << std::endl;
}

int main() {
    Employee arr[3];
    for (int i = 0; i < 3; i++) {
        Employee e;
        e.set_details(i+1);
        arr[i] = e;
    }
    for (int i = 0; i < 3; i ++) {
        arr[i].get_details(i);
    }
    return 0;
}
