#include <iostream>
 
class c_polygon{
    protected :
        float width, height;
    public :
        c_polygon() { }
        void setParams(float a, float b) {
            width = a;
            height = b;
        }
        virtual void getArea() {
            std::cout << "No shape selected" << std::endl;
        }	
};
 
class c_rectangle : public c_polygon {
    public :
        c_rectangle() { }
        void getArea() {
            float area = width * height;
            std::cout << "\nLength : " << height << "  Breadth : " << width;
            std::cout << "\nArea of Rectangle : " << area << std::endl;
        }
};
 
class c_triangle : public c_polygon {
    public :
        c_triangle() { }
    void getArea() {
        float area = 0.5 * width * height;
        std::cout << "\nBase : " << width << "  Height : " << height;
        std::cout << "\nArea of Triangle : " << area << std::endl;
    }
};
 
int main(int argc, const char * argv[]) {
    c_polygon *ptr;
    c_rectangle rect;
    c_triangle tr;
    ptr = &rect;
    ptr->setParams(5,7);
    ptr->getArea();
    ptr = &tr;
    ptr->setParams(3,4);
    ptr->getArea();
    return 0;
}
