#include <iostream>

class Complex {
    private:
        double real;
        double imag;
    public:
        Complex() {
            real = 0;
            imag = 0;
        }
        Complex(double x, double y) {
            real = x;
            imag = y;
        }
        void print() {
            std::cout << real << " + i(" << imag << ")" << std::endl; 
        }
        Complex operator+(Complex &x) {
            double r = this->real + x.real;
            double i = this->imag + x.imag;
            return Complex(r, i);
        }
        Complex operator-(Complex &x) {
            double r = this->real - x.real;
            double i = this->imag - x.imag;
            return Complex(r, i);
        }
        Complex operator*(Complex &x) {
            double r = (this->real * x.real) - (this->imag * x.imag);
            double i = (this->real * x.imag) + (this->imag * x.real);
            return Complex(r, i);
        }
        Complex operator/(Complex &x) {
            double r = (this->real * x.real + this->imag * x.imag) / (x.real * x.real + x.imag * x.imag);
            double i = (this->imag * x.real - this->real * x.imag) / (x.real * x.real + x.imag * x.imag);
            return Complex(r, i) ;
        }
};

int main() {
    Complex a = Complex(1, 2);
    std::cout << "a = ";
    a.print();
    Complex b = Complex(3, 4);
    std::cout << "b = ";
    b.print();
    Complex sum = a + b;
    Complex difference = a - b;
    Complex product = a * b;
    Complex quotient = a / b;
    std::cout << "a + b = ";
    sum.print();
    std::cout << "a - b = ";
    difference.print();
    std::cout << "a * b = ";
    product.print();
    std::cout << "a / b = ";
    quotient.print();
    return 0;
}