#include<iostream>
 
template<typename T>
class Pair {
    private:
        T a;
        T b;
    public:
        Pair(T x,T y=0) {
            a=x;
            b=y;
        }	
        T get_max() {
            return a>b?a:b;
        }
};

int main() {
    Pair <int> p1 (10,13);
    std::cout << "Max Num is : " << p1.get_max() << std::endl;
    Pair <float> p2 (12.1,11.9);
    std::cout << "Max Num is : " << p2.get_max() << std::endl;
    return 0;
}
