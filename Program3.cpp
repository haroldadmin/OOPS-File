#include <iostream>

class CountInstances {
    private:
        static int counter;
    public:
        CountInstances() {
            counter++;
        }
        ~CountInstances() {
            counter--;
        }
        static int get_count() {
            return counter;
        }
};

int CountInstances::counter = 0;

int main() {
    std::cout << "Creating object c1" << std::endl;
    CountInstances c1;
    std::cout << "Creating object c2" << std::endl;
    CountInstances c2;
    std::cout << "Creating object c3" << std::endl;
    CountInstances c3;
    std::cout << "Creating object c4" << std::endl;
    CountInstances c4;
    std::cout << "Destroying object c1" << std::endl;
    c1.~CountInstances();
    std::cout << "Destroying object c2" << std::endl;
    c2.~CountInstances();
    std::cout << "Objects currently in existence: " << CountInstances::get_count() << std::endl;
    return 0;
}