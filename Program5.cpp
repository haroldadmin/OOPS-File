#include <iostream>

inline int add(int, int);
inline int subtract(int, int);
inline int multiply(int, int);
inline int divide(int, int);

inline int add(int a, int b) {
    return a + b;
}

inline int subtract(int a, int b) {
    return a - b;
}

inline int multiply(int a, int b) {
    return a * b;
}

inline int divide(int a, int b) {
    return a/b;
}

int main() {
    int a = 5, b = 10;
    std::cout << "Add: " << add(a, b) << std::endl;
    std::cout << "Subtract: " << subtract(a, b) << std::endl;
    std::cout << "Multiply: " << multiply(a, b) << std::endl;
    std::cout << "Divide: " << divide(a, b) << std::endl;
    return 0;
}