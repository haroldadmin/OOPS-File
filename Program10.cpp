#include <iostream>
#include <string.h>

class Person {
    protected:
        char name[50];
        int age;
    public:
        Person(char *s, int a) {
            strcpy(name, s);
            age = a;
        }
        void display() {
            std::cout << "Name: " << name << std::endl;
            std::cout << "Age: " << age << std::endl;
        }
};

class Student: public Person {
    protected:
        char type[10];
        char college[50];
    public:
        Student(char *s, int a, char *t, char *c) : Person(s, a) {
            strcpy(type, t);
            strcpy(college, c);
        }
        void display() {
            Person::display();
            std::cout << "Type: " << type << std::endl;
            std::cout << "College: " << college << std::endl;
        }
};

class Btech: public Student {
    private:
        int batch;
    public:
        Btech(char *s, int a, char *t, char *c, int b): Student(s, a, t, c) {
            batch = b;
        }
        void display() {
            Student::display();
            std::cout << "Batch: " << batch << std::endl;
        }
};

int main() {
    Btech john = Btech("John", 18, "Btech", "DTU", 2017);
    john.display();
    return 0;
}